package co.gov.igac.snc.xtfajson.dto;

import java.io.File;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RespuestaDTO {

	private String rutaCarpetaGenerada;
	private String codigoStatus;
	private List<String> outJson;
}
