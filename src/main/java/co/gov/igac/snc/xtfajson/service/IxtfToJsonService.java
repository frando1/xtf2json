package co.gov.igac.snc.xtfajson.service;


import co.gov.igac.snc.xtfajson.dto.PeticionDTO;
import co.gov.igac.snc.xtfajson.dto.RespuestaDTO;
import co.gov.igac.snc.xtfajson.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.xtfajson.exception.ExcepcionPropertiesNoExiste;

public interface IxtfToJsonService {

	public RespuestaDTO convertir(PeticionDTO peticion) throws ExcepcionPropertiesNoExiste, ExcepcionLecturaDeArchivo;
}
