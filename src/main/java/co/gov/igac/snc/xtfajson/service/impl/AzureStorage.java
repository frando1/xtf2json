package co.gov.igac.snc.xtfajson.service.impl;

import static co.gov.igac.snc.xtfajson.util.Utilidades.leerProperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;
import com.azure.storage.common.StorageSharedKeyCredential;
import com.azure.storage.file.datalake.DataLakeDirectoryClient;
import com.azure.storage.file.datalake.DataLakeFileClient;
import com.azure.storage.file.datalake.DataLakeFileSystemClient;
import com.azure.storage.file.datalake.DataLakeServiceClient;
import com.azure.storage.file.datalake.DataLakeServiceClientBuilder;

import co.gov.igac.snc.xtfajson.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.xtfajson.exception.ExcepcionPropertiesNoExiste;

public class AzureStorage {
	
	private String accountName;
	private String clientId;
	private String clientSecret;
	private String tenantId;
	private String accountKey;
	private String contenedor;
	private String rutaDescarga;
	
	private DataLakeServiceClient dataLakeSC;
	
	public AzureStorage() throws ExcepcionPropertiesNoExiste, ExcepcionLecturaDeArchivo{
		this.properties();
		this.getDataLakeServiceClient();
		//this.getDataLakeServiceClient2();
	}
	
	private void getDataLakeServiceClient(){
	
	    String endpoint = "https://" + accountName + ".dfs.core.windows.net";
	    
	    ClientSecretCredential clientSecretCredential = new ClientSecretCredentialBuilder()
		    .clientId(clientId)
		    .clientSecret(clientSecret)
		    .tenantId(tenantId)
		    .build();    
	       
	    DataLakeServiceClientBuilder builder = new DataLakeServiceClientBuilder();	   
	    
	    this.dataLakeSC = builder.credential(clientSecretCredential).endpoint(endpoint).buildClient();
	}
	
	private void getDataLakeServiceClient2(){

	    StorageSharedKeyCredential sharedKeyCredential =
	        new StorageSharedKeyCredential(accountName, accountKey);

	    DataLakeServiceClientBuilder builder = new DataLakeServiceClientBuilder();

	    builder.credential(sharedKeyCredential);
	    builder.endpoint("https://" + accountName + ".dfs.core.windows.net");

	    this.dataLakeSC = builder.buildClient();
	}
	
	public File descargarArchivo(String rutaDirectorio, String nombreArchivo) 
			throws ExcepcionLecturaDeArchivo, ExcepcionPropertiesNoExiste{
		
		DataLakeFileSystemClient fileSystemClient = dataLakeSC.getFileSystemClient(contenedor);
		DataLakeDirectoryClient directoryClient = fileSystemClient.getDirectoryClient(rutaDirectorio);
		DataLakeFileClient fileClient = directoryClient.getFileClient(nombreArchivo);
		File file = new File(rutaDescarga+nombreArchivo);
		OutputStream targetStream;
		try {
			targetStream = new FileOutputStream(file);
			fileClient.read(targetStream);
			targetStream.close();
		} catch (FileNotFoundException ex) {
			throw new ExcepcionPropertiesNoExiste(ex.getMessage());
		} catch (IOException ex) {
			throw new ExcepcionLecturaDeArchivo(ex.getMessage());
		}
		//fileClient.flush(file.length());
		return file;
	}
	
	public DataLakeFileSystemClient pruebas(){
		
		return dataLakeSC.getFileSystemClient(contenedor);
	}
	
	private void properties() throws ExcepcionPropertiesNoExiste, ExcepcionLecturaDeArchivo{		
		Properties properties = new Properties();
		leerProperties(properties);
		
		this.accountName = properties.getProperty("accountName");
		this.clientId = properties.getProperty("clientId");
		this.clientSecret = properties.getProperty("clientSecret");
		this.tenantId = properties.getProperty("tenantId");
		this.accountKey = properties.getProperty("accountKey");
		this.contenedor = properties.getProperty("contenedor");
		this.rutaDescarga = properties.getProperty("rutaDescarga");
	}
}
