package co.gov.igac.snc.xtfajson.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import co.gov.igac.snc.xtfajson.exception.ExcepcionLecturaDeArchivo;
import co.gov.igac.snc.xtfajson.exception.ExcepcionPropertiesNoExiste;

public class Utilidades {

	public static void leerProperties(Properties properties) 
			throws ExcepcionPropertiesNoExiste, ExcepcionLecturaDeArchivo {
		try {
			properties.load(new FileReader("D:\\properties\\properties.properties"));
		} catch (FileNotFoundException ex) {
			throw new ExcepcionPropertiesNoExiste(ex.getMessage());
		} catch (IOException ex) {
			throw new ExcepcionLecturaDeArchivo(ex.getMessage());
		}
	}
}
