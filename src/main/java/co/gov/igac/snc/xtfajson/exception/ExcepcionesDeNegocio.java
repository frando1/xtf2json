package co.gov.igac.snc.xtfajson.exception;

public class ExcepcionesDeNegocio extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ExcepcionesDeNegocio(String mensaje) {
		super(mensaje);
	}

}
