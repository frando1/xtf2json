package co.gov.igac.snc.xtfajson.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import co.gov.igac.snc.xtfajson.dto.EstandarDeExcepcionesDTO;

/**
 * 
 * @author jdrodriguezo
 * @version 1.0
 */
@RestControllerAdvice
public class AplicacionEstandarDeExcepciones {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(Exception ex){
		ex.printStackTrace();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("Exception")
				.titulo("Error interno")
				.codigo("E100")
				.estado(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR))
				.detalle(ex.getMessage())
				.instancia("/convertir/xtfToJsonRdm")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ExcepcionPropertiesNoExiste.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(ExcepcionPropertiesNoExiste ex){
		ex.printStackTrace();
		HttpStatus estado = ex.getEstado() == null ? HttpStatus.NOT_IMPLEMENTED : ex.getEstado();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("FileNotFoundException")
				.titulo("Error de archivo no encontrado")
				.codigo("E100")
				.estado(String.valueOf(estado.value()))
				.detalle(ex.getMessage())
				.instancia("/convertir/xtfToJsonRdm")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, estado);
	}
	
	@ExceptionHandler(ExcepcionLecturaDeArchivo.class)
	public ResponseEntity<EstandarDeExcepcionesDTO> handleNoContentException(ExcepcionLecturaDeArchivo ex){
		ex.printStackTrace();
		HttpStatus estado = ex.getEstado() == null ? HttpStatus.NOT_IMPLEMENTED : ex.getEstado();
		EstandarDeExcepcionesDTO respuesta = new EstandarDeExcepcionesDTO.ExceptionBuilder()
				.tipo("IOException")
				.titulo("Error de lectura de archivo")
				.codigo("E200")
				.estado(String.valueOf(estado.value()))
				.detalle(ex.getMessage())
				.instancia("/convertir/xtfToJsonRdm")
				.builder();
		return new ResponseEntity<EstandarDeExcepcionesDTO>(respuesta, estado);
	}
}
